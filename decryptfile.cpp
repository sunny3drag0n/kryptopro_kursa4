#include "DecryptFile.h"

bool decrypt_file(QString address, QString password, HCRYPTKEY hSessionKey){

    FILE *file;              // Исходный файл
    FILE *decFile;              // Зашифрованный файл

    BYTE pbContent[BLOCK_LENGTH] = { 0 };	// Указатель на содержимое исходного файла
    DWORD cbContent = 0;					// Длина содержимого

    if(password == ""){
        password = "default";
    }


    //Задаем вектор инициализкации
    if(!CryptSetKeyParam(hSessionKey,KP_IV, (BYTE *)password.toLocal8Bit().data(),0))
    {
        QMessageBox::critical(NULL,QObject::tr("Ошибка"), "Ошибка установки вектора инициализации.");
        return 1;
    }

    file = fopen(address.toLocal8Bit().data(), "r");
    if (!file){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"), "Ошибка открытия файла для дешифрования.");
        return 1;
    }

    decFile = fopen((address + ".decrypt").toLocal8Bit().data(), "wb");
    if (!decFile){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"), "Ошибка открытия файла для результата дешифрования.");
        return 1;
    }

    do
    {
        memset(pbContent, 0, sizeof(pbContent));
        cbContent = (DWORD)fread(pbContent, 1, BLOCK_LENGTH , file);
        pbContent[cbContent] = '\0';

        if (cbContent)
        {
            BOOL bFinal = feof(file);
            // Дешифроние прочитанного блока на сессионном ключе.
            if (CryptDecrypt(hSessionKey, 0, bFinal, 0, (BYTE*)pbContent, &cbContent))
            {
                // Запись дешифрованного блока в файл.
                if (!fwrite(pbContent, 1, cbContent, decFile))
                {
                    QMessageBox::critical(NULL,QObject::tr("Ошибка"), "Ошибка записи дешифрованного блока.");
                    fclose(decFile);
                    fclose(file);
                    return 1;
                }
            }
            else
            {
                QMessageBox::critical(NULL,QObject::tr("Ошибка"), "Ошибка при дешифровании блока.");
                fclose(decFile);
                fclose(file);
                return 1;
            }
        }
        else
        {
            QMessageBox::critical(NULL,QObject::tr("Ошибка"), "Ошибка чтения блока из файла.");
            fclose(decFile);
            fclose(file);
            return 1;
        }
    } while (!feof(file));

    fclose(decFile);
    fclose(file);

    return 0;
}
