#ifndef ENCRYPTFILE_H
#define ENCRYPTFILE_H

#endif // ENCRYPTFILE_H

#include <QString>      // для работы со строками Qt
#include <QMessageBox>

#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#include <fstream>
#include "defines.h"

bool encrypt_file(
        QString address,    // адрес шифруемого файла
        QString password,   // фраза для вектора инициализации
        HCRYPTKEY hSessionKey   //дискриптор ключа
        );
