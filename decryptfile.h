#ifndef DECRYPTFILE_H
#define DECRYPTFILE_H

#endif // DECRYPTFILE_H

#include <QString>
#include <QMessageBox>

#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#include <fstream>
#include "defines.h"

// Функцией расшифрования файла является функция зашифрования с CryptDecrypt вместо CryptEncrypt
bool decrypt_file(QString address, QString password, HCRYPTKEY hSessionKey);
