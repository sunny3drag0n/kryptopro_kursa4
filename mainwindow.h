#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>  // для использования сообщений
#include <QFileDialog>  // для использования диалогов поиска файлов

#include <stdio.h>      // для использования потоков
#include <windows.h>    // для использования типов переменних msdn
#include <wincrypt.h>   // криптографическая библиотека
#include <fstream>      // для работы с файлами

#include "EncryptFile.h"    // заголовочный файл функции шифрования
#include "DecryptFile.h"    // заголовочный файл функции рашифрования

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    HCRYPTPROV hProvForSign;        // Дескриптор CSP для подписи.
    HCRYPTPROV hProvForSessionKey;  // Дескриптор CSP для шифрования.

    HCRYPTKEY hKey;                 // Дескриптор ключеваой пары подписи
    HCRYPTKEY hPubKey;              // Дескриптор открытого ключа подписи
    HCRYPTKEY hSessionKey;          // Дескриптор сессионного ключа


    HCRYPTHASH hHash;               // Дискриптор Хэша

    BYTE *pbKeyBlob;// экспортированный открытый ключ

    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_Use_Container_clicked();    // создание/выбор контейнера

    void on_Select_Address_clicked();   // выбора файла

    void on_Sing_clicked();             // подписание файла

    void on_Check_Sing_clicked();       // проверка подписи

    void on_Encrypt_clicked();          // шифрование файла

    void on_Decrypt_clicked();          // расшифрование файла

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
